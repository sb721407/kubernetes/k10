# SkillBox DevOps. Kubernetes. Часть 10. Service mesh. Знакомство с Istio и Envoy

SkillBox DevOps. Kubernetes. Часть 10. Service mesh. Знакомство с Istio и Envoy

--------------------------------------------------------

## Задание 1. Создание правил для балансировки трафика

https://gitlab.com/sb721407/kubernetes/k10/-/blob/main/lesson-3/dr.yaml 

https://gitlab.com/sb721407/kubernetes/k10/-/blob/main/lesson-3/vs_reviews_80.yaml

Скриншот с результатами выполнения Задания 1:

https://gitlab.com/sb721407/kubernetes/k10/-/blob/main/png/task_1.png

## Задание 2. Использование заголовков запросов для роутинга

https://gitlab.com/sb721407/kubernetes/k10/-/blob/main/lesson-3/vs_reviews_header.yaml

Скриншот с результатами выполнения Задания 2:

https://gitlab.com/sb721407/kubernetes/k10/-/blob/main/png/task_2.png


